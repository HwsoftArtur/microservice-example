package org.example.fulltestingexampleservice.register;

import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.common.models.RegisterResult;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Optional;

@RequiredArgsConstructor
public class RegisterPersistanceOperation {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Optional<RegisterResult> findById(String userId) {
        return namedParameterJdbcTemplate.query(
                "SELECT user_id, login, status FROM registration WHERE user_id=:id",
                new MapSqlParameterSource("id", userId),
                new BeanPropertyRowMapper<>(RegisterResult.class)
        ).stream().findFirst();
    }

    public void save(RegisterResult registerResult) {
        namedParameterJdbcTemplate.update(
                "INSERT INTO registration(user_id,login,status) VALUES (:userId,:login,:status)",
                new BeanPropertySqlParameterSource(registerResult)
        );
    }
}
