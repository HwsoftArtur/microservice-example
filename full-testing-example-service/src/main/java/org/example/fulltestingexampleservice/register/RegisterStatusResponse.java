package org.example.fulltestingexampleservice.register;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder
public class RegisterStatusResponse {
    String userId;
    Result result;

    public enum Result {
        OK,FAIL
    }
}
