package org.example.fulltestingexampleservice.register;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.fulltestingexampleservice.common.models.RegisterResult;

import java.util.Map;


@RequiredArgsConstructor
@Slf4j
public class RegisterService {
    private final RegisterPersistanceOperation persistanceOperation;
    private final Map<String,RegisterHandler> handlerMap;


    public RegisterResult getResult(String userId) {
        return persistanceOperation.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("Tickets with userId " + userId + " not found"));
    }

    public void register(RegisterEvent registerEvent) {
        RegisterResult registerResult = new RegisterResult();
        registerResult.setLogin(registerEvent.getLogin());
        registerResult.setUserId(registerEvent.getUserId());
        registerResult.setStatus("OK");

        if(handlerMap.containsKey(registerEvent.getSystem())){
            try {
                handlerMap.get(registerEvent.getSystem()).register(
                        registerEvent.getLogin(),
                        registerEvent.getPassword()
                );
            }catch (Exception e){
                log.warn("Error registration {}",registerEvent,e);
                registerResult.setStatus("FAIL");
            }
        }else{
            log.warn("Error registration {}, system {} not found",registerEvent,registerEvent.getSystem());
            registerResult.setStatus("FAIL");
        }
        persistanceOperation.save(registerResult);
    }
}
