package org.example.fulltestingexampleservice.register;

import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.common.models.RegisterResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/external/register")
@RequiredArgsConstructor
public class RegisterController {

    private final RegisterService registerService;

    @GetMapping("{userId}")
    RegisterStatusResponse getRegisterStatus(@PathVariable("userId") String userId){
        RegisterResult result = registerService.getResult(userId);

        return RegisterStatusResponse.builder()
                .result("OK".equals(result.getStatus())? RegisterStatusResponse.Result.OK : RegisterStatusResponse.Result.FAIL)
                .userId(result.getUserId())
                .build();
    }
}
