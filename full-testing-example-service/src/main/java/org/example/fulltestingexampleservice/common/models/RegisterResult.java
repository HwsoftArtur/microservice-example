package org.example.fulltestingexampleservice.common.models;

import lombok.Data;

@Data
public class RegisterResult {
    String userId;
    String login;
    String status;
}
