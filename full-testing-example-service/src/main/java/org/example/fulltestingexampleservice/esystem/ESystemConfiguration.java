package org.example.fulltestingexampleservice.esystem;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ESystemConfiguration {
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    ESystemClient eSystemClient(
            RestTemplate restTemplate,
            @Value("${services.e-system.url}") String url
    ) {
        return new ESystemClient(restTemplate, url);
    }
}
