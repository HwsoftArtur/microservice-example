package org.example.fulltestingexampleservice.esystem.models;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
public class ESystemRegisterResponse {
    Result result;
    String error;

    public enum Result {
        OK,FAIL
    }
}
