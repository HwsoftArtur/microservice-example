package org.example.fulltestingexampleservice.esystem;


import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;
import org.example.fulltestingexampleservice.register.RegisterHandler;

@RequiredArgsConstructor
public class ESystemService implements RegisterHandler {
    private final ESystemClient eSystemClient;


    @Override
    public void register(String login, String password) {
        ESystemRegisterResponse response = eSystemClient.register(
                ESystemRegisterRequest.builder()
                        .login(login)
                        .password(password)
                        .build()
        );

        if(response.getResult()!= ESystemRegisterResponse.Result.OK){
            throw new IllegalArgumentException(response.getError());
        }
    }

    @Override
    public String systemName() {
        return "e-system";
    }
}
