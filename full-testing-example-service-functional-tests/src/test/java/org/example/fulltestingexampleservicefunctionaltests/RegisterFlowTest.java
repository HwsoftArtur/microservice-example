package org.example.fulltestingexampleservicefunctionaltests;

import org.example.fulltestingexampleservice.register.RegisterStatusResponse;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RegisterFlowTest extends BaseFunctionalTest {


    @Test
    void shouldGetSuccessRegisterResult() {
        RegisterStatusResponse expected = RegisterStatusResponse.builder()
                .userId(USER_ID)
                .result(RegisterStatusResponse.Result.OK)
                .build();

        registerSteps.insertRegistrationResult(USER_ID, LOGIN, "OK");

        RegisterStatusResponse response = registerSteps.getStatusResponse(USER_ID);

        assertThat(response).isEqualTo(expected);
    }
}
