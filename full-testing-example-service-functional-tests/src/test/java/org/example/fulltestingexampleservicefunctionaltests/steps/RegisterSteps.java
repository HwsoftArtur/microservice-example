package org.example.fulltestingexampleservicefunctionaltests.steps;

import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.common.models.RegisterResult;
import org.example.fulltestingexampleservice.register.RegisterEvent;
import org.example.fulltestingexampleservice.register.RegisterPersistanceOperation;
import org.example.fulltestingexampleservice.register.RegisterStatusResponse;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Optional;

@RequiredArgsConstructor
public class RegisterSteps {

    private final RegisterPersistanceOperation registerPersistanceOperation;
    private final TestRestTemplate restTemplate;
    private final KafkaTemplate<String,Object> kafkaTemplate;
    private final String registerTopicName;


    public void insertRegistrationResult(String userId, String login, String status){
        RegisterResult registerResult = new RegisterResult();
        registerResult.setStatus(status);
        registerResult.setLogin(login);
        registerResult.setUserId(userId);
        registerPersistanceOperation.save(registerResult);
    }


    public RegisterStatusResponse getStatusResponse(String userId){
        return restTemplate.getForEntity("/api/v1/external/register/"+userId,RegisterStatusResponse.class)
                .getBody();
    }


    public void sendRegisterMessage(String s, String userId, String login, String password) {
        RegisterEvent registerEvent = new RegisterEvent();
        registerEvent.setLogin(login);
        registerEvent.setSystem(s);
        registerEvent.setUserId(userId);
        registerEvent.setPassword(password);

        kafkaTemplate.send(registerTopicName,registerEvent);
    }

    public Optional<RegisterResult> selectState(String userId) {
        return registerPersistanceOperation.findById(userId);
    }
}
