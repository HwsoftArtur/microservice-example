package org.example.fulltestingexampleservicefunctionaltests.steps;


import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@RequiredArgsConstructor
public class ESystemSteps {

    private final JsonSteps jsonSteps;

    public void stubSuccessRegistration(String login, String password) {
        ESystemRegisterResponse response = ESystemRegisterResponse.builder()
                .result(ESystemRegisterResponse.Result.OK)
                .build();

        ESystemRegisterRequest request = ESystemRegisterRequest.builder()
                .login(login)
                .password(password)
                .build();

        stubFor(
                post("/register")
                        .withRequestBody(equalToJson(jsonSteps.toJson(request)))
                        .willReturn(okJson(jsonSteps.toJson(response)))
        );
    }

}
